# Computadores Quanticos
Por conta da necessidade de processamentos de grande volume de dados, os computadores com processadores convencionais estão perdendo espaço. Por conta dessa demanda de potência, a tecnologia está avançando em direção as estruturas quânticas e o uso de novas técnicas de manuseio de elementos químicos, dando origem a uma nova visão computacional da computação quântica.
# O que são?
A capacidade atual de um computador quântico é de 16 qubit (bits quânticos), com isso consegue-se fazer cálculos complexos em curtos períodos de tempo. A Diferença de um qubit binário de um computador quântico e um binário de um computador convencional é que no computador quântico os valores de 0 e 1 são interpretados simultaneamente, diferente de um computador convencional que apenas interpreta um por vez, tudo isso baseando-se em medidas quânticas como as direções de polarização de fótons , os níveis de energia de energia de agrupamentos de átomos , etc.


![](https://s2.glbimg.com/oXvYeMpihABKom1bkf6BNJSDbBU=/0x0:750x500/984x0/smart/filters:strip_icc/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2018/n/y/ZXzPRnR8GT6yBLJPej0Q/2000q-systems-in-lab-for-website-0.jpg)

(Computador quântico da empresa D`Wave)

#Referências
1. O futuro da computação - Processamento quântico. Brayner Tecnologia. Disponível em: http://www.brayner.com.br/post/o-futuro-da-computação-processamento-quântico. Acesso em: 09 mai. 2021.
2. O que é computação quântica?  Brasil Escola. Disponível em: https://brasilescola.uol.com.br/o-que-e/fisica/o-que-e-computacao-quantica.htm#:~:text=Computa%C3%A7%C3%A3o%20qu%C3%A2ntica%20%C3%A9%20a%20ci%C3%AAncia%20que%20estuda%20o,sistemas%20qu%C3%A2nticos,%20como%20%C3%A1tomos,%20f%C3%B3tons%20ou%20part%C3%ADculas%20subat%C3%B4micas. Acesso em:  09 mai. 2021.
