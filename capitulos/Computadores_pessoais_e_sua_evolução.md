# Computadores pessoais e sua evolução
   O tamanho e o preço dos computadores começaram a diminuir a partir da década de 50. Neste período, incia-se a pesquisa dos circuitos integrados, os chips, sendo responsável pela diminuição de tamanho dos aparelho elêtronico. Em 1974, foi projetado pela intel um microprocessador capaz de armazenar em um chip todas as funções processador central tecnologia que permite a invenção do computador pessoal. O primeiro computador pessoal é o Apple I, inventado pelos americanos Stephan Wozniak e Steve Jobs.
   Em 1976 Steve Jobs e Stephan Wozniak fundaram uma pequena empresa numa garagem  se chamada apple onde construíram o Apple I, um ano depois com um projeto novo e melhorado surgiu o Apple II o primeiro microcomputador com grande sucesso comercial e mais tarde o Apple III.
   
   ![Apple I](https://www.hardware.com.br/filters:format:(jpeg)/@/static/20180412/apple-2.jpg)
  
  Em 1981, a IBM lança o seu PC que se tornou um sucesso comercial. O sistema operacional usado é o MS-DOS que foi desenvolvido pela empresa de softwares Microsoft. Na época Bill Gates(dono da Microsoft) convenceu a IBM e as demais companhias a adotarem o sistema operacional de sua empresa. Permitindo que um mesmo programa funcione em micros de vários fabricantes. Posteriormente, os PCs passaram a usar microprocessadores cada vez mais potentes: 286, 386SX, 386DX, 486SX, 486DX. Nos anos 90 surge o Pentium que atualmente é  o processador mais avançado usados em PCs.

  Na década de 90 surgem os computadores que, além do processamento de dados, reúnem fax, modem, secretária eletrônica, scanner, acesso à Internet e drive para CD-ROM.  Em 1996 é anunciado o lançamento do DVD, que nos próximos anos deve substituir o CD-ROM e as fitas de videocassete. O DVD é um compact-disc com uma capacidade de 4,7 gigabytes (cerca de 7 CDs-ROM). Segundo os fabricantes, terá a capacidade de vídeo de um filme de 135 minutos em padrão de compressão MPEG (tela cheia) e alta qualidade de áudio. Será reproduzido em um driver específico, que também poderá ser ligado à televisão. Alguns CDs-ROM são interativos, ou seja, podendo interagir com o menu . Os computadores portáteis como o laptop que é uma marca da miniaturização da tecnologia também se popularizou na década de 90.
  ## A evolução do primeiro computador comparado com atualmente
   Como se tem observado o que  sofreu maiores alterações foi o hardware, tem como base o primeiro computador eletrônico ENIAC, ele  ocupava  uma área superior 170 m2, tinha o peso de 30 toneladas, utilizando 18.000 válvulas e 10.000 capacitores, consumindo 150.000 watts de energia e fazendo o custo de energia ser vários milhões de dólares. Além do tamanho, sua preparação demorava semanas, pois a programação era realizada pela ligação de fios. Atualmente, os microcomputadores pesam alguns quilos e possuem capacidade infinitamente superior ao ENIAC, tendo um custo menor, podendo ser possuído por várias pessoas .
   
   ## Referências:
   
   GADELHA, julia. A evolução dos computadores. Instituto de computação,2021. Disponível em:<http://www.ic.uff.br/~aconci/evolucao.html>. Acesso em 08 de maio de 2021.
   
   Histórico. DCA,2021. Disponível em <https://www.dca.ufrn.br/~affonso/DCA800/pdf/historico.pdf>. Acesso em 08 de maio de 2019.
