# Primeiros computadores

Neste capítulo, será abordado sobre primeiros computadores, onde foram criados e quais eram suas finalidades. Estes aparelhos, que estão constatemente presentes em nosso cotidiano, foram de grande ajuda para o avanço da humanidade em todas as áreas de pesquisa, pois com ele, foi possível dar um grande passo para a evolução da tecnologia no mundo.

## Mark I 

Os primeiros computadores surgiram na mesma época em regiões diferentes, mas em 1944, nos Estados Unidos,foi criado o Mark I, uma calculadora gigante pesando aproximadamente 4,5 toneladas, executava uma multiplicação em seis segundos. A máquina construída por Howard Aiken em conjunto com a IBM, foi a primeira calculadora calculadora automática produzida em larga escala nos Estados Unidos.

## Eniac

Desenvolvido na universidade da Pensilvânia em 1945, O Eniac foi um computador feito sob "encomenda" pelo Exército dos Estados Unidos para calcular trajetórias balísticas de lançamentos de mísseis, pois para fazer esses cálculos manualmente, demandaria muito tempo de soldados. Esta máquina que pesava 30 toneladas, contendo cerca de 18 mil válvulas, fazia 4500 operações por segundo, usava arquitetura decimal ao invés da binária e armazenava 20 números de 10 digítos.
Começou a ser utilizado no final de 1945, mas como nao foi utilizado na guerra foi realocado e religado novamente, funcionando até meados de 1955, nesse meio tempo, foi também utilizado para o desenvolvimento da primeira bomba de hidrogênio desenvolvida pelos EUA.
