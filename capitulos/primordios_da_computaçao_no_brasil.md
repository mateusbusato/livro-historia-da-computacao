# Primórdios da Computação no Brasil
## Relações com Fatos Internacionais 
Um dos primeiros marcos na historia da computação Mundial onde Helmut Theodor Schreyer, engenheiro eletricista, auxiliou seu colega Konrad Zuse em um projeto de construção do primeiro computador constituído por componentes mecânicos e eletromecânicos.


![Konrad Zuse](https://2k8fuf10pw8w25f6ce428ify-wpengine.netdna-ssl.com/ModernComputer/Relays/images/ZuseAndSchreyer1.jpg)


Schreyer foi professor do Instituto Militar de Engenharia e da PUC antiga Escola de Engenharia depois transformada em Centro Tecnológico. Helmut também publicou um livro  no Rio de Janeiro, pela editora ETE, sobre “Computadores Eletrônicos Digitais” 

## Computadores Instalados no Brasil
No decreto de Juscelino Kubitschek , que foi marcado pelo amplo desenvolvimento econômico , pela tentativa de um salto industrial e um intenso processo inflacionário. Nessa tentativa de revolução de modernidade foi caracterizada pela importação de tecnologias de países mais avançados.

O Grupo Executivo para Aplicações de Computadores Eletrônicos(GEACE) e o Conselho Nacional de Desenvolvimento Científico e Tecnológico(CNPq),  foram quem deram inicio ao processo de importação do computador, B-205 da Burroughs.

Em 1960 foi inaugurado o primeiro computador da América Latina em Universidades e o primeiro no Brasil , no Centro de Processamento de Dados da PUC-RJ.

## COMPUTADORES PROJETADOS NO BRASIL
Os primeiros protótipos de computadores surgiram nas universidades nacionais como projetos de conclusão dos cursos de engenharia. Foi com esses projetos que ocorreu um avanço no desenvolvimento tecnológico do país.

### Lourinha
No Instituto Militar de Engenharia ,no final de 1960, foi criado, pelos alunos, um computador chamado de “Lourinha” que continha parte digital e analógica capazes de resolverem até mesmo equações diferenciais.

### Zezinho
Em 1961, quatro alunos do Instituto Tecnológico da Aeronáutica, depois de terem feito uma visita à Cie. De Machine Bull, fizeram um Computador batizado de “Zezinho”, ele foi apresentado como trabalho de conclusão de curso deles, era um equipamento didático que mostrava como funcionava o processamento dentro de um computador .

### Patinho Feio 
Em 1972, foi inaugurado o “Patinho Feio” no Laboratório de Sistemas Digitais da Escola Politécnica da Universidade de São Paulo. Feito com 100% de seus recursos oriundos da Escola Politécnica o projeto teve êxito em seus objetivos , que eram de treinar e formar jovens com habilidades de projetar e implementar computadores. Seu nome veio de uma brincadeira com um projeto da Marinha chamado de Cisne Branco.

## Referências 
CARDI, M.L.;BARRETO, J. M. Primórdios da Computação no Brasil. Florianópolis.Universidade Federal de Santa Catarina.
