# Surgimento das calculadoras Mecânicas

A invenção das calculadoras foi um grande avanço para a área de exatas, pois com ela facilitasse um trabalho que levaria algum tempo para ser feito. Já imaginou um engenheiro sem uma calculadora? Ou até mesmo um físico tendo que escrever cálculos extensos sem o auxilio dessa ferramenta? Estamos aqui para falar sobre este aparelho que revolucionou as nosssas vidas.

## Ábaco

Acredita-se que em meados do século VI a.c, na china era descoberto o ábaco, um objeto com fios paralelos que continham arruelas ou contas, que, dependendo das suas posições, apresentavam resultados diferentes. Com o tempo, ele chegou aos romanos, que nomearam esse conjunto de fios e contas de "calculi", dando origem a palavra cálculo. Apesar de ser um objeto bastante limitado, o ábaco era bastante util para realizar operações básicas, por isso foi usado durante 24 décadas.

## Primeiras calculadoras

Em 1642, Blaise Pascal, filho de um cobrador de impostos resolveu ajudar seu pai a resolver as contas dos tributos a serem pagos. Com base no ábaco, inventou uma máquina rápida e eficiente capaz de fazer cálculos de adição e subtração, assim dando a idéia da primeira calculadora mecânica.
Passados alguns anos, o alemão Gottfried Wilhelm von Leibniz - filósofo e matemático - desenvolveu um mecanismo capaz de realizar outras operações, deu o nome de "roda graduada", mas, por causa de seu custo alto de fabricação, apenas algumas pessoas podiam usar este instrumento.

## Calculadoras Mecânicas

Com o passar dos anos, houve uma carência de calculadoras de menor valor e mais compactas, para que fosse levadas para diversos lugares sem maiores esforços. Assim, em 1947, foi desevolvido o primeiro projeto de calculadora mecânica pelo austríaco Curt Herzstark. As vendas de seus aparelhos foram mantidas até 1973, quando surgiram as calculadoras eletrônicas.


# Referências:
GADELHA, Julia. A evolução dos computadores. Disponível em:http://www.ic.uff.br/~aconci/evolucao.html#:~:text=Em%201946%2C%20surge%20o%20Eniac,realiza%204.500%20c%C3%A1lculos%20por%20segundo. Acessado em: 07 de maio de 2021.

História da calculadora. Disponível em:http://clickeaprenda.uol.com.br/portal/mostrarConteudo.php?idPagina=29427#:~:text=Ent%C3%A3o%2C%20em%201947%2C%20o%20austr%C3%ADaco,ao%20tamanho%20de%20um%20copo.&text=As%20vendas%20de%20suas%20calculadoras,quando%20surgiram%20as%20calculadoras%20eletr%C3%B4nicas. Acessado em: 07 de maio de 2021.

