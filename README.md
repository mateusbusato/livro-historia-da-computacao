# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/primeiros_computadores.md)
1. [Evolução dos Computadores Pessoais e sua Evolução](capitulos/Computadores_pessoais_e_sua_evolução.md)
1. [Primórdios da Computação no Brasil](capitulos/primordios_da_computaçao_no_brasil.md)
1. [Computadores Quânticos](capitulos/computadores_quanticos.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| | Mateus Busato | mateusbusato | [mateusbusato@alunos.utfpr.edu.br](mailto:mateusbusato@alunos.utfpr.edu.br)
| | Rafael Boldrini Demezuk| RafaelDemezuk | [rafaeldemezuk@hotmail.com](mailto:rafaeldemezuk@hotmail.com)
| | Marcio Greco Junior| marciogrco | [marciogrecojunior@hotmail.com](mailto:marciogrecojunior@hotmail.com)
